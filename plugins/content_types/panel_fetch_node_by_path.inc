<?php
/**
 * @file
 * Contains ctools plugin for Panel fetch node by path module.
 */

/**
 * Fetch node by path plugin pane.
 */
$plugin = array(
  'title' => t('Panel fetch node by path'),
  'description' => t('Fetch the node by url'),
  'single' => TRUE,
  'content_types' => 'panel_fetch_node_by_path_content_pane',
  'render callback' => 'panel_fetch_node_by_path_content_pane_render',
  'edit form' => 'panel_fetch_node_by_path_edit_form',
  'admin info' => 'panel_fetch_node_by_path_fetch_node_by_path_admin_info',
  'category' => array(t('Miscellaneous'), -9),
);


/**
 * Plugin render function.
 */
function panel_fetch_node_by_path_content_pane_render($subtype, $conf, $args, $context) {
  // Fetch node by path.
  $path = drupal_lookup_path("source", $conf['path']);

  if ($path !== FALSE) {
    $node = menu_get_object("node", 1, $path);

    $block = new stdClass();

    // The title actually used in rendering.
    $block->title = '';
    $block->content = node_view($node);
    return $block;
  }
  else {
    drupal_set_message(t('The path set in the pane settings for fetch node by path are not in the system', 'error'));
    watchdog('Panel fetch node by path', 'Path not in system');
    return FALSE;
  }
}

/**
 * Admin info callback for panel pane.
 */
function panel_fetch_node_by_path_fetch_node_by_path_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass();
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';
    $block->content = t('This pane fetch a node by path and render it as full viewmode.');
    return $block;
  }
}

/**
 * Panel fetch node by path edit form.
 */
function panel_fetch_node_by_path_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['path'] = array(
    '#title' => t('Node path to fetch'),
    '#description' => t('The node path to fetch'),
    '#type' => 'textfield',
    '#default_value' => $conf['path'],
    '#required' => TRUE,
  );
  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function panel_fetch_node_by_path_fetch_node_by_path_edit_form_submit($form, &$form_state) {
  $form_state['conf']['path'] = $form_state['values']['path'];
}

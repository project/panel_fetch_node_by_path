INTRODUCTION
------------

This module will add the functionality to panels to fetch node by path and
render it in full viewmode.

INSTALLATION
------------

* Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

* Go to your panel page and add a new pane. 
  In the "Miscellaneous" category you find "Panel fetch node by path"
  in the settings of that pane you can add the path to the node to render.

Done!
